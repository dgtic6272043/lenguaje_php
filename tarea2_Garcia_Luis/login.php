<?php
session_start(); // Iniciar o reanudar la sesión

// Leer el archivo JSON y convertirlo en un arreglo PHP
if (file_exists('usuarios.json')) {
    $usuarios = json_decode(file_get_contents('usuarios.json'), true);
} else {
    $usuarios = []; // Inicializar como un arreglo vacío si el archivo no existe
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['n_cuenta']) && isset($_POST['contrasena'])) {
    // Obtener los datos del formulario
    $numeroCuentaBuscado = $_POST['n_cuenta'];
    $contrasenaBuscada = $_POST['contrasena'];

    // Búsqueda del usuario
    $usuarioEncontrado = null;
    foreach ($usuarios as $usuario) {
        if ($usuario['numero_cuenta'] === $numeroCuentaBuscado && $usuario['contrasena'] === $contrasenaBuscada) {
            $usuarioEncontrado = $usuario;
            break;
        }
    }

    // Procesar el resultado de la búsqueda
    if ($usuarioEncontrado !== null) {
        // Guardar datos del usuario en la sesión
        $_SESSION['usuario'] = $usuarioEncontrado;
        
        // Redirigir a info.php
        header('Location: info.php');
        exit;
    } else {
        echo "<script>alert('Usuario no encontrado o contraseña incorrecta.');</script>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>login</title>
    <link rel="stylesheet" href="login.css">
</head>
<body>
    <div class="login-container">
        <h1>Login</h1>
        <form method="post" action="">
            <label for="numcuenta">Número de Cuenta:</label>
            <input type="text" id="numcuenta" name="n_cuenta">
            <label for="contra">Contraseña:</label>
            <input type="password" id="contra" name="contrasena">
            <button type="submit">Entrar</button>
        </form>
    </div>
</body>
</html>


