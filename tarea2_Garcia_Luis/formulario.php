<?php

session_start(); // Asegúrate de iniciar la sesión al principio del script

// Verificar si el usuario está logueado y si es el usuario con número de cuenta 1
if (!isset($_SESSION['usuario']) || $_SESSION['usuario']['numero_cuenta'] !== '1') {
    // Si no está logueado o no es el usuario con número de cuenta 1, redirigir a info.php
    header('Location: info.php');
    exit;
}



if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Leer el archivo JSON y convertirlo a un arreglo PHP
    if (file_exists('usuarios.json')) {
        $usuarios = json_decode(file_get_contents('usuarios.json'), true);
    } else {
        $usuarios = []; // Inicializar como un arreglo vacío si el archivo no existe
    }



    // Recopilar datos del formulario
    $nuevoUsuario = [
        'numero_cuenta' => $_POST['numcuenta'],
        'nombre' => $_POST['name'],
        'primer_apellido' => $_POST['firstSurname'],
        'segundo_apellido' => $_POST['secondSurname'],
        'genero' => $_POST['gender'],
        'fecha_nacimiento' => $_POST['birthdate'],
        'contrasena' => $_POST['contra']
    ];

    // Agregar el nuevo usuario al array
    $usuarios[] = $nuevoUsuario;

    // Guardar el arreglo actualizado en el archivo JSON
    file_put_contents('usuarios.json', json_encode($usuarios));

    // Redirigir a info.php
    header('Location: info.php');
    exit;
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>formulario</title>
<link rel="stylesheet" href="formulario.css">
</head>
<body>
<div class="header">
  <a href="info.php">Home</a>
  <a href="formulario.php">Registrar Alumnos</a>
  <a href="logout.php">Cerrar Sesión</a>
</div>

<div class="register-container">
  <form class="register-form" method="post" action="">
    <label for="password">Numero de cuenta</label>
    <input type="text" id="accountNumber" name="accountNumber">
    <label for="password">Nombre</label>
    <input type="text" id="name" name="name">
    <label for="password">Apellido</label>
    <input type="text" id="firstSurname" name="firstSurname">
    <label for="password">Segundo apellido</label>
    <input type="text" id="secondSurname" name="secondSurname">
    <fieldset>
      <legend>Género</legend>
      <label><input type="radio" name="gender" value="hombre"> Hombre</label>
      <label><input type="radio" name="gender" value="mujer"> Mujer</label>
      <label><input type="radio" name="gender" value="otro"> Otro</label>
    </fieldset>

    <label for="birthdate">Fecha de Nacimiento</label>
    <input type="text" id="birthdate" name="birthdate" placeholder="dd/mm/aaaa">

    <label for="password">Contraseña</label>
    <input type="password" id="password" name="password">

    <button type="submit">Registrar</button>
  </form>
</div>
</body>
</html>
