<?php
session_start();


if (!isset($_SESSION['usuario'])) {
    
    header('Location: login.php');
    exit;
}

if (file_exists('usuarios.json')) {
    $usuarios = json_decode(file_get_contents('usuarios.json'), true);
} else {
    $usuarios = []; 
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>info</title>
    <link rel="stylesheet" href="info.css">
</head>
<body>
<div class="header">
  <a href="info.php">Home</a>
  <a href="formulario.php">Registrar Alumnos</a>
  <a href="logout.php">Cerrar Sesión</a>
</div>

<div class="auth-container">
    <h2>> Usuario Autenticado <</h2>
        <p><strong><?php echo htmlspecialchars($_SESSION['usuario']['nombre'] . " " .  $_SESSION['usuario']['primer_apellido']); ?></strong></p>
        <p>Información: </p>
        <div class="info">
        <p>Número de cuenta: <?php echo htmlspecialchars($_SESSION['usuario']['numero_cuenta']); ?></p>
        <p>Fecha de Nacimiento: <?php echo htmlspecialchars($_SESSION['usuario']['fecha_nacimiento']); ?></p>
    </div>
        <h3>Datos guardados:</h3>
           <table>
        <tr>
            <th>#Número de cuenta</th>
            <th>Nombre</th>
            <th>Fecha de Nacimiento</th>
        </tr>

        <?php foreach ($usuarios as $usuario): ?>
            <tr>
                <td><?php echo htmlspecialchars($usuario['numero_cuenta']); ?></td>
                <td><?php echo htmlspecialchars($usuario['nombre']) . ' ' . htmlspecialchars($usuario['primer_apellido']) . ' ' . htmlspecialchars($usuario['segundo_apellido']); ?></td>
                <td><?php echo htmlspecialchars($usuario['fecha_nacimiento']); ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</body>
</html>