<?php


if (isset($_POST['email'])) {
	  $email = $_POST['email'];
    $patron = "/^[a-zA-Z0-9._%+-]+@(gmail|hotmail|outlook|comunidad.unam)\.(com|es|mx|org)$/";

    if (preg_match($patron, $email)) {
      $r_mail = "El correo electronico es válido.";
    } else {
      $r_mail = "El correo electronico es invalido.";
    }
}
elseif (isset($_POST['curp'])) {
	  $CURP = $_POST['curp'];
    $patron = "/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/" ;

    if (preg_match($patron, $CURP)) {
      $r_curp = "El CURP es válido.";
    } 
    else {
      $r_curp = "El CURP es invalido.";
    }
}
elseif (isset($_POST['50_p'])) {
	$texto = $_POST['50_p'];
  $patron = "/\b[a-zA-Z]{51,}\b/";

  if (preg_match($patron, $texto, $palabra_larga)) {
    $r_50_p = "Palabra con mas de 50 caracteres encontrada: " . $palabra_larga[0];
  } else {
    $r_50_p = "No se encontraron palabras con más de 50 caracteres.";
  }
}
elseif (isset($_POST['escapar'])) {
	function escapar($texto) {
    $caractEsp = ['\\', '^', '$', '.', '|', '?', '*', '+', '(', ')', '[', ']', '{', '}', '$','%'];
    foreach ($caractEsp as $caracter) {
        $texto = str_replace($caracter, '', $texto);
    }
    return $texto;
  }

  $textoOriginal = $_POST["escapar"];
  $textoEscapado = escapar($textoOriginal);

  $r_escapar = $textoEscapado;
}
elseif (isset($_POST['decimal'])) {
	$numero = $_POST["decimal"];
  $patron = "/^\d+(\.\d+)?$/";

  if (preg_match($patron, $numero)) {
    $r_decimal = "El número es un decimal.";
  } else {
    $r_decimal = "El número no es un decimal.";
  }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulario con Espacio de Mensajes</title>
    <style type="text/css">
body {
    font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
    background-color: #eaeaea;
    padding-top: 50px; 
    margin: 0;
    min-height: 100vh;
}

.barra-superior {
    width: 100%;
    height: 50px; 
    background-color: #333;
    color: white;
    position: fixed;
    top: 0;
    left: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 16px;
}

.conte {
    background-color: #fff;
    border-radius: 10px;
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
    padding: 20px;
    margin: 10px; 
    width: calc(15% - 10px); 
    display: inline-block; 
}

.conte h2 {
    color: #333;
    font-size: 20px;
    margin-bottom: 10px;
}

.conte p {
    color: #555;
    font-size: 14px;
    margin-bottom: 15px;
}

form {
    margin-bottom: 15px;
}

input[type="text"] {
    width: 100%;
    padding: 8px;
    border: 1px solid #ccc;
    border-radius: 4px;
    margin-bottom: 10px;
}

button {
    background-color: #007bff;
    color: white;
    padding: 8px 12px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    font-size: 14px;
    display: block; 
    margin: 0 auto; 
}

button:hover {
    background-color: #0056b3;
}

#respuesta_email, #respuesta_curp, #respuesta_50p, #respuesta_escapar, #respuesta_decimales {
    color: #333;
    padding: 10px 0;
    font-size: 14px;

}

@media (max-width: 768px) {
    .conte {
        width: 100%; 
    }
}



    </style>
</head>
<body>
    <div class="barra-superior">
      <h1> Tarea2 - Expresiones Regulares - García Rodríguez Luis Armando</h1>
    </div>

    <div class="conte">
        <h2> E-mail</h2>
        <p>Realizar una expresión regular que detecte emails correctos</p>
        <form id="formulario" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
            <input type="text" id="input_email" placeholder="Ingresa un E-mail" name="email">
            <button type="submit">Enviar</button>
        </form>
        <div id="respuesta_email">
          <?php
          if (isset($r_mail)){
            echo $r_mail;
          }
          ?>
        </div>
    </div>
    <div class="conte">
        <h2> CURP</h2>
        <p>Realizar una expresion regular que detecte CURPS Correctos</p>
        <form id="formulario" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
            <input type="text" id="input_curp" placeholder="Ingresa un CURP" name="curp">
            <button type="submit">Enviar</button>
        </form>
        <div id="respuesta_curp">
          <?php
          if (isset($r_curp)){
            echo $r_curp;
          }
          ?>
        </div>
    </div>
    <div class="conte">
        <h2> 50 palabras</h2>
        <p>Realizar una expresion regular que detecte palabras de longitud mayor a 50</p>
        <form id="formulario" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
            <input type="text" id="input_50p" placeholder="Ingresa un texto" name="50_p">
            <button type="submit">Enviar</button>
        </form>
        <div id="respuesta_50p">
          <?php
          if (isset($r_50_p)){
            echo $r_50_p;
          }
          ?>
        </div>
    </div>
    <div class="conte">
        <h2> Escapar palabras</h2>
        <p>Crea una funcion para escapar los simbolos especiales</p>
        <form id="formulario" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
            <input type="text" id="input_escapar" placeholder="Ingresa un texto" name="escapar">
            <button type="submit">Enviar</button>
        </form>
        <div id="respuesta_escapar">
          <?php
          if (isset($r_escapar)){
            echo $r_escapar;
          }
          ?>
        </div>
    </div>
    <div class="conte">
        <h2> Decimales</h2>
        <p>Crear una expresion regular para detectar números decimales</p>
        <form id="decimales" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" >
            <input type="text" id="input_decimales" placeholder="Ingresa un texto" name="decimal">
            <button type="submit">Enviar</button>
        </form>
        <div id="respuesta_decimales">
          <?php
          if (isset($r_decimal)){
            echo $r_decimal;
          }

          ?>
        </div>
    </div>
</body>
</html>