<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Ejercicos PHP</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f4f4f4;
        }

        header {
            background: #333;
            color: white;
            padding: 10px 0;
            text-align: center;
        }
        h2 {
            background:#f4f4f4;
            color: red;
            padding: 10px 0;
            text-align: center;
        }

        ul {
           background:#f4f4f4;
            color: green;
            padding: 10px 0;
            text-align: center; 
        }
    </style>
</head>
<body>
    <header>
        <h1>
        <?php 
        echo "Tarea1 - Piramide y Rombo - García Rodríguez Luis Armando ";
        ?>  
        </h1>
    </header>

    <section id="Ejercicios">
        <div class="Ej">
            <h2><center>Ejercicio piramide</center> </h2>
            <ul>
             <?php 
               $filas = 30;
               for ($i = 1; $i <= $filas; $i++){
                echo "<center>" . str_repeat("X", $i) . "</center>";
               }
            ?>
           </ul>  
        </div>

        <div class ="Ej">
            <h2><center>Ejercicio rombo</center> </h2>
            <ul>
            <?php

               $filas= 30;
               for ($i = 1; $i < $filas; $i++){
                echo "<center>" . str_repeat("X", $i) . "</center>";
               }

               for ($n = 1; $n < $filas; $n++){
                $j = $filas -1 ;
                $k = $j-$n;
                echo "<center>" . str_repeat("X", $k) . "</center>";
                } 
             ?>
             
            </ul>
        </div>
    </section>

    

    <footer></footer>
</body>
</html>
